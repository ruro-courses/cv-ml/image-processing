import itertools

import numpy as np

from common import register_command
from denoise import do_denoise_once
from filters import bilateral, median
from noise import make_noisy, mse

noise_search_space = [1, 2, 5, 12, 28, 64]
median_r_search_space = [0, 1, 2, 3]
bilateral_d_search_space = [0, 1, 2, 4]
bilateral_r_search_space = [4, 8, 16, 32, 64, 128]


@register_command
def search_bilateral(original: np.ndarray) -> object:
    # Helper functions, used for generating calibration/*.npy
    noisy_images = [
        (nl, make_noisy(nl, original))
        for nl in noise_search_space
    ]
    results = np.zeros((
        len(noisy_images),
        len(median_r_search_space),
        len(bilateral_d_search_space),
        len(bilateral_r_search_space)
    ))

    import tqdm
    pb = tqdm.tqdm(total=results.size)

    for (
            (n_idx, (nl, ni)),
            (m_idx, mr),
    ) in itertools.product(
        enumerate(noisy_images),
        enumerate(median_r_search_space),
    ):
        restored_m = median(mr, ni)
        for (
                (d_idx, bd),
                (r_idx, br),
        ) in itertools.product(
            enumerate(bilateral_d_search_space),
            enumerate(bilateral_r_search_space),
        ):
            restored_b = bilateral(bd, br, restored_m)
            results[n_idx, m_idx, d_idx, r_idx] = mse(original, restored_b)
            pb.update()

    return results


@register_command
def search_noise_level(original: np.ndarray) -> object:
    # Helper functions, used for generating calibration/*.npy
    noisy_images = [
        (nl, make_noisy(nl, original))
        for nl in noise_search_space
    ]
    results = np.zeros(len(noisy_images))

    import tqdm
    pb = tqdm.tqdm(total=results.size)

    for n_idx, (nl, ni) in enumerate(noisy_images):
        restored = do_denoise_once(ni, nl)
        results[n_idx] = np.sqrt(mse(ni, restored))
        pb.update()

    return results

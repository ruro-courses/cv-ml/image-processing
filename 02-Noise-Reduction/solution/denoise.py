import numpy as np

from common import quantize, register_command
from filters import bilateral, median
from query import get_noise_level, query


def do_denoise_once(image, noise_level):
    mr, bd, br = query(noise_level)
    image = median(mr, image)
    image = bilateral(bd, br, image)
    return quantize(image)


def do_denoise_iter(
        image,
        start_noise=64, momentum=0.2,
        stop_iter=100, stop_delta=0.5,
):
    assert stop_iter > 0
    delta = np.inf
    n_iter = 0
    denoised = None
    noise_level = start_noise

    while n_iter < stop_iter and abs(delta) > stop_delta:
        denoised = do_denoise_once(image, noise_level)
        next_noise_level = get_noise_level(image, denoised)

        next_delta = next_noise_level - noise_level
        if n_iter and np.sign(next_delta) == np.sign(delta):
            delta = momentum * delta + next_delta
        else:
            delta = next_delta

        noise_level += delta
        n_iter += 1

    return denoised, noise_level


@register_command
def calc_noise(image: np.ndarray):
    return do_denoise_iter(image)[1]


@register_command
def denoise(image: np.ndarray) -> np.ndarray:
    return do_denoise_iter(image)[0]

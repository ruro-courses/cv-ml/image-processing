import numpy as np

from common import quantize, register_command


@register_command
def mse(image1: np.ndarray, image2: np.ndarray):
    image1 = image1.astype(np.float)
    image2 = image2.astype(np.float)
    delta = (image1 - image2)
    mse_val = np.einsum('ij,ij->', delta, delta) / delta.size
    return mse_val


@register_command
def make_noisy(noise_level: float, image: np.ndarray) -> np.ndarray:
    noise = np.random.normal(0, noise_level, size=image.shape)
    return quantize(image + noise)

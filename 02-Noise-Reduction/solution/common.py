import argparse
import inspect
import os
import pickle
import sys

import cv2
import numpy as np

commands = set()
output_types = {np.ndarray, object}


def register_command(fun):
    commands.add(fun)
    return fun


def run():
    # Parse command line args
    cline = argparse.ArgumentParser()
    mode = cline.add_subparsers(dest='mode', required=True)

    signatures = {}
    for fun in commands:
        cmd = mode.add_parser(fun.__name__)
        sig = inspect.getfullargspec(fun)

        for name in sig.args:
            meta = sig.annotations[name]
            if meta == np.ndarray:
                cmd.add_argument(name, type=read_grayscale)
            else:
                cmd.add_argument(name, type=meta)

        ret = sig.annotations.get('return', None)
        if ret in output_types:
            cmd.add_argument("output")

        assert fun.__name__ not in signatures
        signatures[fun.__name__] = (
            fun, sig.args, ret
        )

    args = cline.parse_args()
    fun, sig, ret = signatures[args.mode]
    sig = {s: getattr(args, s) for s in sig}
    return dump_output(ret, fun(**sig), getattr(args, 'output', None))


def read_grayscale(path):
    if not (os.path.exists(path) and os.path.isfile(path)):
        raise FileNotFoundError("Input image doesn't exist")

    img = cv2.imread(path)
    if img is None:
        raise cv2.error("OpenCV could not read the image")

    if img.ndim == 2:
        return img
    if img.ndim != 3:
        raise ValueError("Image must have 2 or 3 dimensions")

    if img.shape[2] == 1:
        return img.squeeze(2)
    if img.shape[2] != 3:
        raise ValueError("Image must have 1 or 3 channels")

    if not (
            np.all(img[..., 0] == img[..., 1]) and
            np.all(img[..., 0] == img[..., 2])
    ):
        print(
            "Warning: Color images are not supported.",
            "The image will be converted to grayscale before processing.",
            file=sys.stderr
        )

    return img.mean(axis=2).round().clip(0, 255).astype(np.uint8)


def quantize(image):
    dt = image.dtype
    if dt == np.uint8:
        return image
    if dt == np.float:
        image = np.round(image, out=image)
        image = np.clip(image, 0, 255, out=image)
        image = image.astype(np.uint8)
        return image
    raise AssertionError(dt)


def dump_output(otype, result, path=None):
    if otype == np.ndarray:
        assert cv2.imwrite(path, quantize(result))
    elif otype == object:
        with open(path, 'wb') as f:
            pickle.dump(result, f)
    else:
        if isinstance(result, tuple):
            print(*result)
        else:
            print(result)

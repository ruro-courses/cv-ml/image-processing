import denoise  # noqa: F401
import filters  # noqa: F401
import noise  # noqa: F401
import query  # noqa: F401
import search  # noqa: F401
from common import run

if __name__ == '__main__':
    exit(run())

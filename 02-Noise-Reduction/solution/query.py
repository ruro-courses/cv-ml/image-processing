import numpy as np
import scipy.interpolate

from common import register_command
from noise import mse

median_r_curve = np.load('calibration/median_r.npy')
bilateral_r_curve = np.load('calibration/bilateral_r.npy')
bilateral_d_curve = np.load('calibration/bilateral_d.npy')
rmse_to_noise_curve = np.load('calibration/rmse_to_noise.npy')


def interp_curve(p, c, extrapolate=True, whole=False):
    if extrapolate:
        v = scipy.interpolate.interp1d(*c, fill_value='extrapolate')(p)
    else:
        v = np.interp(p, *c)

    v = v.clip(0, None)
    return v.round().astype(np.int) if whole else v


def get_noise_level(image1, image2):
    mse_val = np.sqrt(mse(image1, image2))
    noise_level = interp_curve(mse_val, rmse_to_noise_curve)
    return noise_level


@register_command
def query(noise_level: float):
    median_r = interp_curve(noise_level, median_r_curve, whole=True)
    bilateral_r = interp_curve(noise_level, bilateral_r_curve)
    bilateral_d = interp_curve(noise_level, bilateral_d_curve)
    return median_r, bilateral_d, bilateral_r

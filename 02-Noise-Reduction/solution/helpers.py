import numpy as np

sqrt_2pi = np.sqrt(2 * np.pi)


def shift_view(img, h, w):
    # Create h * w views, which represent a shifting window into the image
    img = np.ascontiguousarray(img)
    ih, iw = img.shape
    es = img.dtype.itemsize
    sh, sw = img.strides
    vh, vw = ih - h + 1, iw - w + 1

    return np.ndarray(
        (h, w, vh, vw),
        strides=(iw * es, es, sh, sw),
        buffer=img, dtype=img.dtype
    )


def gaussian_fn(x, sigma, out=None):
    norm_inner = -1 / (2 * sigma ** 2)
    norm_outer = 1 / (sigma * sqrt_2pi)

    x = np.multiply(x, x, out=out)
    x = np.multiply(norm_inner, x, out=x)
    x = np.exp(x, out=x)
    x = np.multiply(norm_outer, x, out=x)

    return x


def make_gaussian_1d(sigma, radius_mult=3, min_radius=2, add_radius=0):
    radius = add_radius + np.maximum(
        min_radius,
        np.ceil(radius_mult * sigma).astype(int)
    ).item()
    x = np.linspace(-radius, radius, num=2 * radius + 1)
    gaussian = gaussian_fn(x, sigma)
    gaussian /= gaussian.sum()
    return gaussian[np.newaxis]

import numpy as np

from common import register_command
from helpers import gaussian_fn, make_gaussian_1d, shift_view


def get_d_weights(sigma_d):
    g_flat = make_gaussian_1d(sigma_d)
    g_2d = g_flat * g_flat.T
    return g_2d, g_flat.size


def get_r_weights(sigma_r, image, surround):
    deltas = image[np.newaxis, np.newaxis] - surround
    return gaussian_fn(deltas, sigma_r, out=deltas)


def bilateral_core(image, image_surround, sigma_r, d_w, out=None):
    r_w = get_r_weights(sigma_r, image, image_surround)
    filtered = np.einsum('ijHW,ijHW,ij->HW', image_surround, r_w, d_w, out=out)
    filtered /= np.einsum('ijHW,ij->HW', r_w, d_w)
    return filtered


@register_command
def bilateral(sigma_d: float, sigma_r: float, image: np.ndarray) -> np.ndarray:
    if sigma_d < 0.25 or sigma_r < 0.25:
        return image

    d_w, d = get_d_weights(sigma_d)

    padding, rem = np.divmod(d - 1, 2)
    padding = padding.repeat(2, axis=0)
    padding = padding[:, np.newaxis].repeat(2, axis=1)
    padding[:, 1] += rem

    image = image.astype(np.float)
    image_padded = np.pad(image, padding, mode='symmetric')
    image_surround = shift_view(image_padded, d, d)

    normed = np.empty_like(image)
    oh, ow = normed.shape

    # Perform core bilateral computation in chunks to limit peak memory usage
    chunk_size = np.ceil(2 ** 26 / (ow * d * d)).astype(int)
    for i in range(0, oh, chunk_size):
        chunk = slice(i, i + chunk_size)
        bilateral_core(
            image[chunk], image_surround[:, :, chunk],
            sigma_r, d_w,
            out=normed[chunk]
        )

    return normed


@register_command
def median(r: int, image: np.ndarray) -> np.ndarray:
    image_padded = np.pad(image, ((r, r), (r, r)), mode='symmetric')
    image_surround = shift_view(image_padded, 1 + 2 * r, 1 + 2 * r)
    return np.median(image_surround, axis=(0, 1))

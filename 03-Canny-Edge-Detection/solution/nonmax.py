import numpy as np

from common import grad_to_hsv, register_command, yes_no
from convolve import shift_view
from gradient import gradient


def nms(image, sigma):
    value, angle, direction = gradient(image, sigma)

    nms_angle = np.zeros_like(angle, dtype=np.float)
    nms_value = np.zeros_like(value, dtype=np.float)

    value_padded = np.pad(value, (1, 1), mode='symmetric')
    (
        (nw, nn, ne),
        (ww, cc, ee),
        (sw, ss, se),
    ) = shift_view(value_padded, 3, 3)
    for mask in [
        (direction == 1) & (cc > ww) & (cc > ee),
        (direction == 2) & (cc > nn) & (cc > ss),
        (direction == 3) & (cc > ne) & (cc > sw),
        (direction == 4) & (cc > nw) & (cc > se),
    ]:
        nms_value[mask] = value[mask]
        nms_angle[mask] = angle[mask]

    return nms_value, nms_angle


@register_command
def nonmax(
        image: np.ndarray, sigma: float, color: yes_no
) -> np.ndarray:
    value, angle = nms(image, sigma)
    if color:
        return grad_to_hsv(value, angle)
    else:
        return value

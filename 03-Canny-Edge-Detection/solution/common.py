import argparse
import inspect

import numpy as np

commands = set()


def register_command(fun):
    commands.add(fun)
    return fun


def str2bool(v: str):
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')


class Choice:
    def __init__(self, *options, required=True, convert=str):
        self.convert = convert
        self.options = options
        self.required = required


def parse_args():
    # Parse command line args
    cline = argparse.ArgumentParser()
    mode = cline.add_subparsers(dest='mode', required=True)
    cline.add_argument("input")
    cline.add_argument("output")

    signatures = {}
    for fun in commands:
        cmd = mode.add_parser(fun.__name__.rstrip('_'))
        sig = inspect.getfullargspec(fun)

        for name in sig.args[1:]:
            meta = sig.annotations[name]
            if isinstance(meta, Choice):
                opts = {'choices': meta.options, 'type': meta.convert}
                if meta.required:
                    cmd.add_argument(name, **opts)
                else:
                    cmd.add_argument(
                        "--" + name, **opts,
                        default=meta.options[0]
                    )
            else:
                cmd.add_argument(name, type=meta)

        assert fun.__name__.rstrip('_') not in signatures
        signatures[fun.__name__.rstrip('_')] = (
            fun, sig.args[1:], sig.annotations['return']
        )

    args = cline.parse_args()
    fun, sig, ret = signatures[args.mode]
    sig = {s: getattr(args, s) for s in sig}
    assert ret in {np.ndarray, list}
    return args.input, args.output, fun, sig, ret


yes_no = Choice(False, True, required=False, convert=str2bool)


def quantize(image):
    dt = image.dtype
    if dt == np.uint8:
        return image
    if dt == np.float:
        image = image.copy()
        image = np.round(image, out=image)
        image = np.clip(image, 0, 255, out=image)
        image = image.astype(np.uint8)
        return image
    raise AssertionError(dt)


hsv_order = [[3, 1, 0, 0, 2, 3], [2, 3, 3, 1, 0, 0], [0, 0, 2, 3, 3, 1]]


def grad_to_hsv(v, a):
    # Bonus: compute the direction of the gradient and return that as color
    v = v / 255
    h = 3 * (1 + a / np.pi)

    i = np.floor(h)
    f = h - i
    i = i.astype(int) % 6

    p = np.zeros((4,) + h.shape)
    p[1] = v * (1 - f)
    p[2] = v * f
    p[3] = v

    rgb = np.zeros(h.shape + (3,))
    for c, o in enumerate(hsv_order):
        nsi = np.take(o, i, axis=0)
        rgb[..., c] = np.choose(nsi, p)

    return 255 * rgb

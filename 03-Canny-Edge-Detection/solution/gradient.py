import numpy as np

from common import grad_to_hsv, quantize, register_command, yes_no
from convolve import convolve
from gauss import make_gaussian_1d

sobel = np.asarray([[-1, 0, 1]])
direction_map = np.asarray([0, 1, 4, 2, 3])


def quantize_angles(angles, wrap_angles=True, angle_bins=4):
    angles = angles.copy()
    if wrap_angles:
        angles %= np.pi
        angles /= np.pi
    else:
        angles += np.pi
        angles /= 2 * np.pi
    directions = quantize(angles * angle_bins)
    directions[np.isclose(directions, angle_bins)] = 0
    return 1 + directions


def gradient(image, sigma):
    if sigma < 0.25:
        # See the implementation of `gauss` for an explanation
        dx = convolve(image, sobel)
        dy = convolve(image, sobel.T)
    else:
        # for small sigma values, "pad" the gaussian a little bit along the
        # derivative kernel axis to increase precision
        wide_gauss = make_gaussian_1d(sigma, add_radius=1)
        ss = convolve(wide_gauss, sobel)

        gauss = make_gaussian_1d(sigma)
        dx = convolve(convolve(image, ss), gauss.T)
        dy = convolve(convolve(image, ss.T), gauss)

    value = np.sqrt(dx * dx + dy * dy)
    value *= 255 / value.max()

    # Angles are only valid if gradient is not 0
    valid = quantize(value) != 0
    angle = np.arctan2(dy, dx)
    quant_angle = quantize_angles(angle)

    # The directional mapping required in the task
    # is kinda weird so we need to map the values
    direction = np.zeros(value.shape, dtype=np.float)
    direction[valid] = direction_map[quant_angle][valid]

    return value, angle, direction


@register_command
def dir_(
        image: np.ndarray, sigma: float, color: yes_no
) -> np.ndarray:
    value, angle, direction = gradient(image, sigma)
    if color:
        return grad_to_hsv(value, angle)
    else:
        return 64 * direction

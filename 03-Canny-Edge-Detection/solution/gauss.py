import numpy as np

sqrt_2pi = np.sqrt(2 * np.pi)


def make_gaussian_1d(sigma, radius_mult=3, min_radius=2, add_radius=0):
    radius = add_radius + np.maximum(
        min_radius,
        np.ceil(radius_mult * sigma).astype(int)
    ).item()
    x = np.linspace(-radius, radius, num=2 * radius + 1)
    norm_inner = -1 / (2 * sigma ** 2)
    norm_outer = 1 / (sigma * sqrt_2pi)
    gaussian = norm_outer * np.exp(norm_inner * x * x)
    gaussian /= gaussian.sum()
    return gaussian[np.newaxis]

import cv2
import numpy as np

from common import grad_to_hsv, register_command, yes_no
from convolve import shift_view
from nonmax import nms


def hysteresis(value, hi, lo):
    candidates = value >= lo
    edges = value >= hi

    edges = np.pad(edges, (1, 1), mode='symmetric')
    (
        (nw, nn, ne),
        (ww, cc, ee),
        (sw, ss, se),
    ) = shift_view(edges, 3, 3)

    while np.any(
            # find edges from next step of hysteresis
            new_edges := (
                    # add an edge if it
                    candidates &  # is a candidate (>= lo)
                    ~cc &  # is not already an edge
                    # has an adjacent edge in its 8-neighbourhood
                    (nw | nn | ne | ww | ee | sw | ss | se)
            )
    ):
        cc[...] |= new_edges

    return cc


@register_command
def canny(
        image: np.ndarray,
        sigma: float, thr_high: float, thr_low: float,
        color: yes_no
) -> np.ndarray:
    assert thr_high >= thr_low
    thr_high *= 255
    thr_low *= 255

    value, angle = nms(image, sigma)
    mask = hysteresis(value, thr_high, thr_low)

    edge_value = 255.0 * mask
    if color:
        return grad_to_hsv(edge_value, angle)
    else:
        return edge_value

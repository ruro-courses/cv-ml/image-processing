import numpy as np

from common import Choice, register_command

mirror_axes = Choice('x', 'y')
rotation_directions = Choice('cw', 'ccw')


@register_command
def mirror(
        image: np.ndarray, axis: mirror_axes
) -> np.ndarray:
    if axis == 'x':
        image = image[:, ::-1]
    if axis == 'y':
        image = image[::-1]

    return image


@register_command
def extract(
        image: np.ndarray, left_x: int, top_y: int, width: int, height: int
) -> np.ndarray:
    bottom_y = top_y + height
    right_x = left_x + width

    if not (
            0 <= height and
            0 <= width and
            0 <= top_y < image.shape[0] and
            0 <= left_x < image.shape[1] and
            0 < bottom_y <= image.shape[0] and
            0 < right_x <= image.shape[1]
    ):
        raise ValueError("Invalid extract parameters.")

    image = image[top_y:bottom_y, left_x:right_x]
    assert image.shape == (height, width)
    return image


@register_command
def rotate(
        image: np.ndarray, direction: rotation_directions, angle: int
) -> np.ndarray:
    rot, frac = divmod(angle, 90)
    if frac:
        raise ValueError("Rotation angle should be a multiple of 90 degrees.")
    if direction == 'ccw':
        rot *= -1

    return {
        0: lambda im: im,
        1: lambda im: im.swapaxes(0, 1)[:, ::-1],
        2: lambda im: im[::-1, ::-1],
        3: lambda im: im.swapaxes(0, 1)[::-1, :],
    }[rot % 4](image)

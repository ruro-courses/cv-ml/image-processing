import operator

import numpy as np

from common import Choice, register_command

extension_kinds = Choice('dup', 'even', 'odd')
floor_div = operator.floordiv  # Aka a // b


def ceil_div(a, b, /):
    return -(-a // b)


def zero_div(a, b, /):
    return ceil_div(a, b) if a < 0 else floor_div(a, b)


def extend_single(img, kind, pad):
    img_h, img_w = img.shape
    out_h = img_h + 2 * pad
    out = np.empty((out_h, img_w), dtype=img.dtype)

    edges = {-1: img[:1], 0: img, 1: img[-1:]}
    delta = edges[1] - edges[-1]

    # For each repetition of the source image
    rep = ceil_div(pad, img_h - 1)
    for idx in range(-rep, 1 + rep):
        # Generate helper parameters for this repetition
        odd_idx = idx % 2
        sgn_idx = -1 if odd_idx else 1
        dbl_idx = zero_div(idx, 2)

        # Get the segments of the input and output images, if they are cropped
        lo_out = idx * (img_h - 1) + pad
        hi_out = lo_out + img_h

        lo_out = np.clip(lo_out, 0, out_h)
        hi_out = np.clip(hi_out, 0, out_h)

        lo_img = (lo_out - pad) % (img_h - 1)
        hi_img = lo_img + hi_out - lo_out

        # Select the input, output and edge segments based on parameters
        out_s = out[lo_out:hi_out]
        img_s = img[::sgn_idx][lo_img:hi_img]
        edg_s = edges[np.sign(idx)]

        # Write to the output image
        if kind == 'dup':
            out_s[...] = edg_s
        if kind == 'even':
            out_s[...] = img_s
        if kind == 'odd':
            adjust = dbl_idx * delta + odd_idx * edg_s
            out_s[...] = sgn_idx * img_s + 2 * adjust

    return out


@register_command
def extend(
        image: np.ndarray, kind: extension_kinds, size: int
) -> np.ndarray:
    ###########################################################################
    #  Extend can be implemented in 1 line of numpy, but that's cheating :^)  #
    #                                                                         #
    # return quantize(np.pad(image.astype(np.float), size, **{                #
    #     'dup': {'mode': 'edge'},                                            #
    #     'even': {'mode': 'reflect', 'reflect_type': 'even'},                #
    #     'odd': {'mode': 'reflect', 'reflect_type': 'odd'},                  #
    # }[kind]))                                                               #
    ###########################################################################

    if size < 0:
        raise ValueError("Extension size should be greater than 0")

    if kind == 'odd':
        # odd extension can overflow
        image = image.astype(np.float)

    for _ in range(2):
        image = extend_single(image, kind, size).T

    return image

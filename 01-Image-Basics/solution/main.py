import os
import sys

import cv2
import numpy as np

import affine  # noqa: F401
import extend  # noqa: F401
import gauss  # noqa: F401
import gradient  # noqa: F401
import hist  # noqa: F401
from common import parse_args


def read_grayscale(path):
    if not (os.path.exists(path) and os.path.isfile(path)):
        raise FileNotFoundError("Input image doesn't exist")

    img = cv2.imread(path)
    if img is None:
        raise cv2.error("OpenCV could not read the image")

    if img.ndim == 2:
        return img
    if img.ndim != 3:
        raise ValueError("Image must have 2 or 3 dimensions")

    if img.shape[2] == 1:
        return img.squeeze(2)
    if img.shape[2] != 3:
        raise ValueError("Image must have 1 or 3 channels")

    if not (
            np.all(img[..., 0] == img[..., 1]) and
            np.all(img[..., 0] == img[..., 2])
    ):
        print(
            "Warning: Color images are not supported.",
            "The image will be converted to grayscale before processing.",
            file=sys.stderr
        )

    return img.mean(axis=2).round().clip(0, 255).astype(np.uint8)


def quantize(image):
    dt = image.dtype
    if dt == np.uint8:
        return image
    if dt == np.float:
        image = np.round(image, out=image)
        image = np.clip(image, 0, 255, out=image)
        image = image.astype(np.uint8)
        return image
    raise AssertionError(dt)


def main():
    inp, out, fun, opt, ret = parse_args()

    # Call the registered function
    image = read_grayscale(inp)
    result = fun(image=image, **opt)

    # Save the resulting image
    if ret == np.ndarray:
        assert cv2.imwrite(out, quantize(result))

    # Save the resulting text
    if ret == list:
        with open(out, 'w') as f:
            f.writelines(f'{v}\n' for v in result)


if __name__ == '__main__':
    exit(main())

import numpy as np

from common import Choice, register_command, str2bool
from convolve import convolve
from gauss import make_gaussian_1d

yes_no = Choice(False, True, required=False, convert=str2bool)
sobel = np.asarray([[-1, 0, 1]])
hsv_order = [[3, 1, 0, 0, 2, 3], [2, 3, 3, 1, 0, 0], [0, 0, 2, 3, 3, 1]]


def grad_to_hsv(dx, dy):
    # Bonus: compute the direction of the gradient and return that as color
    v = np.sqrt(dx ** 2 + dy ** 2)
    v /= v.max()
    h = 3 * (1 + np.arctan2(dy, dx) / np.pi)

    i = np.floor(h)
    f = h - i
    i = i.astype(int) % 6

    p = np.zeros((4,) + h.shape)
    p[1] = v * (1 - f)
    p[2] = v * f
    p[3] = v

    rgb = np.zeros(h.shape + (3,))
    for c, o in enumerate(hsv_order):
        nsi = np.take(o, i, axis=0)
        rgb[..., c] = np.choose(nsi, p)

    return 255 * rgb


@register_command
def gradient(
        image: np.ndarray, sigma: float, color: yes_no = False
) -> np.ndarray:
    if sigma < 0.25:
        # See the implementation of `gauss` for an explanation
        dx = convolve(image, sobel)
        dy = convolve(image, sobel.T)
    else:
        # for small sigma values, "pad" the gaussian a little bit along the
        # derivative kernel axis to increase precision
        wide_gauss = make_gaussian_1d(sigma, add_radius=1)
        ss = convolve(wide_gauss, sobel)

        gauss = make_gaussian_1d(sigma)
        dx = convolve(convolve(image, ss), gauss.T)
        dy = convolve(convolve(image, ss.T), gauss)

    if not color:
        grad = np.sqrt(dx * dx + dy * dy)
        grad *= 255 / grad.max()
        return grad
    else:
        return grad_to_hsv(dx, dy)

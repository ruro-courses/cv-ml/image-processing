import numpy as np

from common import register_command
from convolve import convolve

sqrt_2pi = np.sqrt(2 * np.pi)


def make_gaussian_1d(sigma, radius_mult=3, min_radius=2, add_radius=0):
    radius = add_radius + np.maximum(
        min_radius,
        np.ceil(radius_mult * sigma).astype(int)
    ).item()
    x = np.linspace(-radius, radius, num=2 * radius + 1)
    norm_inner = -1 / (2 * sigma ** 2)
    norm_outer = 1 / (sigma * sqrt_2pi)
    gaussian = norm_outer * np.exp(norm_inner * x * x)
    gaussian /= gaussian.sum()
    return gaussian[np.newaxis]


@register_command
def gauss(
        image: np.ndarray, sigma: float
) -> np.ndarray:
    if sigma < 0.25:
        # You can relax this check to be sigma < 0 if you really want, but
        # a gaussian blur with a sigma < 1/4 will most likely be unnoticeable
        raise ValueError(
            "Gauss blur with a sigma less than 1/4 won't change the image."
        )
    gaussian = make_gaussian_1d(sigma)
    image = convolve(image, gaussian)
    image = convolve(image, gaussian.T)
    return image

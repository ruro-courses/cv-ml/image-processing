import argparse
import inspect

import numpy as np

commands = set()


def register_command(fun):
    commands.add(fun)
    return fun


def str2bool(v):
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')


class Choice:
    def __init__(self, *options, required=True, convert=str):
        self.convert = convert
        self.options = options
        self.required = required


def parse_args():
    # Parse command line args
    cline = argparse.ArgumentParser()
    mode = cline.add_subparsers(dest='mode', required=True)
    cline.add_argument("input")
    cline.add_argument("output")

    signatures = {}
    for fun in commands:
        cmd = mode.add_parser(fun.__name__)
        sig = inspect.getfullargspec(fun)

        for name in sig.args[1:]:
            meta = sig.annotations[name]
            if isinstance(meta, Choice):
                opts = {'choices': meta.options, 'type': meta.convert}
                if meta.required:
                    cmd.add_argument(name, **opts)
                else:
                    cmd.add_argument(
                        "--" + name, **opts,
                        default=meta.options[0]
                    )
            else:
                cmd.add_argument(name, type=meta)

        assert fun.__name__ not in signatures
        signatures[fun.__name__] = (
            fun, sig.args[1:], sig.annotations['return']
        )

    args = cline.parse_args()
    fun, sig, ret = signatures[args.mode]
    sig = {s: getattr(args, s) for s in sig}
    assert ret in {np.ndarray, list}
    return args.input, args.output, fun, sig, ret

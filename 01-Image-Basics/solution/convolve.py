import numpy as np


def slice_end(v):
    return v if v < 0 else None


def shift_view(img, h, w):
    # Create h * w views, which represent a shifting window into the image
    sh = [slice(i, slice_end(i + 1 - h)) for i in range(h)]
    sw = [slice(i, slice_end(i + 1 - w)) for i in range(w)]
    return [[img[si, sj] for sj in sw] for si in sh]


def convolve(image, kernel):
    ###########################################################################
    #  Convolve can be implemented in 1 line of scipy, but that's boring :^)  #
    #                                                                         #
    # return scipy.ndimage.convolve(image.astype(np.float), kernel)           #
    ###########################################################################

    kernel_shape = np.asarray(kernel.shape)
    padding, rem = np.divmod(kernel_shape - 1, 2)
    padding = padding[:, np.newaxis].repeat(2, axis=1)
    padding[:, 1] += rem

    # Pad, shift and convolve
    image_padded = np.pad(image, padding, mode='symmetric')
    image_shifted = shift_view(image_padded, *kernel_shape)
    return np.einsum('ijHW,ij->HW', image_shifted, kernel[::-1, ::-1])

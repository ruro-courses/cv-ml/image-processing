import numpy as np

from common import register_command

uint8_max = np.iinfo(np.uint8).max
uint8_bins = np.arange(1 + uint8_max)


def get_hist(img):
    assert img.dtype == np.uint8
    hist = np.zeros(1 + uint8_max, dtype=np.int)
    np.add.at(hist, img.flatten(), 1)
    return hist


@register_command
def calc_hist(
        image: np.ndarray
) -> list:
    hist = [str(h) for h in get_hist(image)]
    assert len(hist) == 1 + uint8_max
    return hist


@register_command
def eq_hist(
        image: np.ndarray
) -> np.ndarray:
    hist = get_hist(image)
    cdf = hist.cumsum()
    cdf = uint8_max / cdf[-1] * cdf
    return np.interp(image, uint8_bins, cdf)

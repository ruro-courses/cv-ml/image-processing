Задание 1: Основы работы с изображениями
========================================

Обязательная часть задания
--------------------------

Должна быть разработана программа, реализующая следующий функционал:

-   Отражение изображения по вертикали и по горизонтали
-   Извлечение фрагмента изображения
-   Поворот изображений по и против часовой стрелки на произвольное
    число градусов, кратное 90
-   Расширение изображения с помощью дублирования пикселей, нечётного и
    чётного продолжения
-   Свёртка с фильтром Гаусса с произвольным выбором параметра —
    радиуса σ
-   Вычисление модуля градиента как корень из суммы квадратов свёрток с
    первой производной фильтра Гаусса по горизонтали и вертикали

Дополительная часть задания
---------------------------

Реализовать следующий функционал:

-   Вычисление гистограммы
-   Эквализация гистограммы

Требования
----------

-   Недопустимо использовать готовые библиотечные функции. Можно: любые
    векторные и матричные операции, включая вычисление свёртки. Ядро
    фильтра Гаусса должно быть вычислено самостоятельно.
-   При выводе результата значения, выходящие за пределы диапазона
    \[0, 255\], должны быть усечены до 0 или 255. В промежуточных
    вычислениях данного усечения быть не должно.
-   При вычислении модуля градиента должно быть произведено
    контрастирование — расширение диапазона с \[0, gmax\] до \[0,
    255\], где gmax — максимальное значение модуля градиента среди
    всего изображения. При этом не должно быть потерь, связанных с
    округлением, на промежуточных этапах.
-   При вычислении свёрток не должен меняться размер изображения. При
    обращении к пикселям, лежащим за пределами изображения, должно быть
    проведено расширение изображения.
-   Результат вычисления гистограммы — текстовый файл, состоящий из
    256 строк, в каждой строке — количество пикселей, имеющих
    интенсивность, равную номеру строки, начиная с 0.

Формат параметров командной строки
----------------------------------

Программа должна поддерживать запуск из командной строки со строго
определённым форматом команд:

```sh
python main.py [command] [parameters]... [input_file] [output_file]
```

Список команд:

|                                             |                                                                                                                                                   |
|:--------------------------------------------|:--------------------------------------------------------------------------------------------------------------------------------------------------|
| `mirror {x/y}`                              | Отражение по горизонтали или по вертикали, в зависимости от указанного параметра                                                                  |
| `extract (left_x) (top_y) (width) (height)` | Извлечение фрагмента изображения                                                                                                                  |
| `rotate {cw/ccw} (angle)`                   | Поворот по или против часовой стрелки на заданное количество градусов, например: `rotate cw 90`                                                   |
| `extend {dup/even/odd} (size)`              | Расширение изображения на `size` пикселей с каждой стороны с использованием дублирования пикселей, чётного и нечётного продолжения соответственно |
| `gauss (sigma)`                             | Фильтр Гаусса, параметр `sigma` — вещественный параметр фильтра                                                                                   |
| `gradient (sigma)`                          | Модуль градиента                                                                                                                                  |
| `calc_hist`                                 | Вычисление гистограммы                                                                                                                            |
| `eq_hist`                                   | Эквализация гистограммы                                                                                                                           |

Примеры изображений
-------------------

|                          |                                                                                             |                                                                                              |
|:------------------------:|:-------------------------------------------------------------------------------------------:|:--------------------------------------------------------------------------------------------:|
| ![](images/source.png)   | ![](images/sobel_horz_2.png)                                                                | ![](images/sobel_vert_1.png)                                                                 |
| Исходное изображение     | Горизонтальный фильтр Собеля                                                                | Вертикальный фильтр Собеля<br/>(изображение предварительно<br/>переведено в градации серого) |
| ![](images/flip_h.png)   | ![](images/rotate_ccw90.png)                                                                | ![](images/median_r2.png)                                                                    |
| Горизонтальное отражение | Поворот на 90 градусов<br/>против часовой стрелки                                           | Медианная фильтрация<br/>с радиусом 2                                                        |
| ![](images/gauss_r3.png) | ![](images/gradient_r1.png)                                                                 |                                                                                              |
| Фильтр Гаусса с σ=3      | Модуль градиента, вычисленный с<br/>помощью свёрток с производными<br/>фильтра Гаусса с σ=1 |                                                                                              |
